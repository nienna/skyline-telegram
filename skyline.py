import random
import bisect
import copy
import logging


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG, filename='skyline.log')
logger = logging.getLogger("skyline.skyline")


class skyline:
    def __init__(self):
        logger.debug("Created Skyline")
        self.area = 0
        self.altura = 0
        self.edificis = []

    # Calcula si el bloc indicad es superposa amb el Skyline
    def essuperposa(self, pos, bloc):
        tipussuper = 0
        blororx = bloc[0]
        blocfix = bloc[2]
        if pos != 0:
            antorx = self.edificis[pos-1][0]
            antfix = self.edificis[pos-1][2]
            # Tot el nou bloc es troba a dins del anterior
            if blororx >= antorx and blocfix <= antfix:
                if bloc[1] <= self.edificis[pos-1][1]:
                    tipussuper = 3
                else:
                    tipussuper = 4
            # Es troba entre el seguent i l'anterior
            elif blororx < antfix and blocfix >= antfix:
                tipussuper = 1
            # No es troba al anterior
            elif blororx >= antfix and blocfix >= antfix:
                if pos != len(self.edificis):
                    tipussuper = 2
                else:
                    tipussuper = 0
        if pos != len(self.edificis) and tipussuper != 3:
            segorx = self.edificis[pos][0]
            segfix = self.edificis[pos][2]
            # Es troba entre el seguent i l'anterior
            if blororx >= segorx and blocfix <= segfix:
                tipussuper = 2
            elif blororx <= segfix and blocfix > segfix:
                tipussuper = 1
            # No es troba al seguent
            elif blororx <= segorx and blocfix <= segorx:
                tipussuper = 0
        return tipussuper

    # Tractament dels casos de superposicio entre multiples blocs
    def superoposA(self, pos, bloc, superpos):
        limitesq = self.edificis[pos-1][0]
        if pos < len(self.edificis):
            limitdret = self.edificis[pos][2]
        else:
            limitdret = self.edificis[pos-1][2]
        # La superposicio es troba nomes entre els dos blocs adjacents
        if bloc[0] > limitesq and (bloc[2] < limitdret or pos == len(self.edificis)):
            # Te un bloc anterior
            if pos != 0:
                # Es mes alt el bloc anterior
                if self.edificis[pos-1][1] >= bloc[1]:
                    bloc[0] = self.edificis[pos-1][2]
                else:
                    self.edificis[pos-1][2] = bloc[0]
            if pos != len(self.edificis):
                # Es mes alt el bloc anterior
                if self.edificis[pos][1] >= bloc[1]:
                    bloc[0] = self.edificis[pos-1][2]
                else:
                    self.edificis[pos][2] = bloc[0]
        else:
            newbloc = [-1, -1, -1]
            if bloc[1] > self.edificis[pos-1][1]:
                if bloc[0] != self.edificis[pos-1][0]:
                    newbloc[2] = self.edificis[pos-1][0]
                    self.edificis[pos-1][2] = bloc[0]
                    newbloc[0] = bloc[0]
                    newbloc[1] = bloc[1]
                else:
                    self.edificis[pos-1][1] = bloc[1]
            it = pos
            while it < len(self.edificis) and bloc[2] > self.edificis[it][2]:
                if self.edificis[it][1] < bloc[1]:
                    self.edificis[it][1] = bloc[1]
                it = it + 1
            if it == len(self.edificis):
                bloc = [self.edificis[it-1][2], bloc[1], bloc[2]]
            else:
                if bloc[1] > self.edificis[it][1]:
                    bloc[0] = self.edificis[it][0]
                    self.edificis[it][0] = bloc[2]
                else:
                    superpos = 3
                    # El bloc no s'insertara
            if newbloc[1] != -1:
                self.edificis.insert(pos, newbloc)

    # Tractament dels casos de superposicio amb un unic bloc
    def superoposB(self, pos, bloc, superpos):
        finou = self.edificis[pos-1][2]
        if finou == bloc[2] and self.edificis[pos-1][0] == bloc[0]:
            self.edificis[pos-1][1] = bloc[1]
            superpos = 3
        elif finou == bloc[2]:
            self.edificis[pos-1][2] = bloc[0]
        elif self.edificis[pos-1][0] == bloc[0]:
            list(self.edificis)
            aux = bloc[2]
            self.edificis[pos-1][0] = aux
        else:
            self.edificis[pos-1][2] = bloc[0]
            self.edificis.insert(pos, [bloc[2], self.edificis[pos-1][1], finou])

    # Funcio per insertar un nou edifici a una skyline
    def insertar_skyline(self, bloc):
        pos = bisect.bisect(self.edificis, bloc)
        superpos = -1
        if self.edificis:  # NO es el primer element a insertar
            superpos = self.essuperposa(pos, bloc)
            print("Supeposicio {x}".format(x=superpos))
            if superpos == 1:
                # Superposa dins de multiples edifici
                self.superoposA(pos, bloc, superpos)
            elif superpos == 2:
                # Superposa dins de un unic edifici, es el seguent
                self.superoposB(pos, bloc, superpos)
            elif superpos == 4:
                # Superposa dins de un unic edifici, es el anterior
                self.superoposB(pos-1, bloc, superpos)
            if superpos != -1 and superpos != 3:
                self.edificis.insert(pos, bloc)
        else:
            # Cas especial del primer edifici ha insertar
            list(bloc)
            self.edificis.append(bloc)

    # Funcio per invertir una Skyline
    def invertir_skyline(self):
        print("Inversio")
        if len(self.edificis) < 2:
            return self
        else:
            it = len(self.edificis) - 1
            poslliure = self.edificis[0][0]
            inverted_skyline = skyline()
            while it >= 0:
                inicibloc = poslliure
                dist = self.edificis[it][0] - poslliure
                fibloc = self.edificis[it][2] - dist
                bloc = [inicibloc, self.edificis[it][1], fibloc]
                poslliure = fibloc
                inverted_skyline.edificis.append(bloc)
                it = it - 1
            inverted_skyline.area = self.area
            inverted_skyline.altura = self.altura
            return inverted_skyline

    # Creacio Simple
    def creacio_unica(self, xmin, altura, xmax):
        self.edificis.append([xmin, altura, xmax+xmin])
        self.area = xmax * altura
        self.altura = altura

    # Creacio Composta
    def creacio_composta(self, edificis):
        for bloc in edificis:
            bloc[2] = bloc[0] + bloc[2]
            self.insertar_skyline(bloc)
        # self.edificis = edificis
        # Eliminar superposicions i calcular area
        for bloc in edificis:
            self.area = self.area + ((bloc[2]-bloc[0])*bloc[1])
            if self.altura < bloc[1]:
                self.altura = bloc[1]

    # Creacio Aleatoria
    def creacio_aleatoria(self, n, h, w, xmin, xmax):
        for x in range(n):
            alturar = random.randint(1, h)  # Alçada 0 no valida
            ampladar = random.randint(1, w)
            inicir = random.randint(xmin, xmax)
            self.insertar_skyline([inicir, alturar, inicir + ampladar])
            self.calcular_area()

    # Calcular el area i la alçada
    def calcular_area(self):
        self.area = 0
        self.altura = 0
        for bloc in self.edificis:
            self.area = self.area + ((bloc[2] - bloc[0]) * bloc[1])
            if self.altura < bloc[1]:
                self.altura = bloc[1]

    # Sobrecarrega del operador +
    def __add__(self, other):
        if isinstance(other, self.__class__):  # Unio de Skylines
            for bloc in other.edificis:
                self.insertar_skyline(bloc)
            self.calcular_area()
            return self
        elif isinstance(other, int):  # Desplaçament de Skylines
            for posicio, bloc in enumerate(self.edificis):
                self.edificis[posicio][0] = self.edificis[posicio][0] + other
                self.edificis[posicio][2] = self.edificis[posicio][2] + other
            return self

    # Sobrecarrega del operador *
    def __mul__(self, other):
        if isinstance(other, self.__class__):  # Interseccio de Skylines
            new_skyline = skyline()
            pos = 0
            for bloc in other.edificis:
                while self.edificis[pos][2] < bloc[0]:
                    pos = pos + 1
                    if pos >= len(self.edificis):
                        return new_skyline
                tipusuperpos = self.essuperposa(pos, bloc)
                newbloc = ["nul", -1, -1]

                if tipusuperpos == 1 or tipusuperpos == 4 or tipusuperpos == 2:
                    if pos+1 < len(self.edificis):
                        limit = self.edificis[pos+1][2]
                    else:
                        limit = self.edificis[pos][2]
                    if tipusuperpos == 1 and bloc[2] > limit:
                        while pos < len(self.edificis) and bloc[2] > self.edificis[pos][0]:
                            if bloc[0] > self.edificis[pos][0]:
                                newbloc[0] = bloc[0]
                            else:
                                newbloc[0] = self.edificis[pos][0]
                            if bloc[1] < self.edificis[pos][1]:
                                newbloc[1] = bloc[1]
                            else:
                                newbloc[1] = self.edificis[pos][1]
                            if bloc[2] < self.edificis[pos][2]:
                                newbloc[2] = bloc[2]
                            else:
                                newbloc[2] = self.edificis[pos][2]
                            new_skyline.edificis.append(copy.copy(newbloc))
                            bloc[0] = self.edificis[pos][2]
                            pos = pos + 1
                    else:
                        if bloc[0] < self.edificis[pos][2]:
                            if bloc[0] > self.edificis[pos][0]:
                                newbloc[0] = bloc[0]
                            else:
                                newbloc[0] = self.edificis[pos][0]
                            if bloc[1] < self.edificis[pos][1]:
                                newbloc[1] = bloc[1]
                            else:
                                newbloc[1] = self.edificis[pos][1]
                            if bloc[2] < self.edificis[pos][2]:
                                newbloc[2] = bloc[2]
                            else:
                                newbloc[2] = self.edificis[pos][2]
                            new_skyline.edificis.append(copy.copy(newbloc))
                        if (pos + 1) < len(self.edificis) and bloc[2] > self.edificis[pos][2]:
                            if(bloc[0] != "null"):
                                bloc[0] = self.edificis[pos][2]
                            if bloc[0] < self.edificis[pos + 1][0]:
                                newbloc[0] = bloc[0]
                            else:
                                newbloc[0] = self.edificis[pos + 1][0]
                            if bloc[1] < self.edificis[pos + 1][1]:
                                newbloc[1] = bloc[1]
                            else:
                                newbloc[1] = self.edificis[pos + 1][1]
                            if bloc[2] < self.edificis[pos + 1][2]:
                                newbloc[2] = bloc[2]
                            else:
                                newbloc[2] = self.edificis[pos + 1][2]
                            new_skyline.edificis.append(newbloc)
            new_skyline.calcular_area()
            return new_skyline
        elif isinstance(other, int):  # Replicacio de Skylines
            replicable = copy.deepcopy(self.edificis)
            posfinal = self.edificis[(len(self.edificis)-1)][2]
            distancia = posfinal - self.edificis[0][0]
            for x in range(0, other-1):
                for y in range(0, len(replicable)):
                    replicable[y][0] = replicable[y][0] + distancia
                    replicable[y][2] = replicable[y][2] + distancia
                    self.edificis.append(copy.copy(replicable[y]))
            self.area = self.area * other
            return self

    # Sobrecarrega del operador -
    def __sub__(self, other):
        if isinstance(other, int):  # Desplaçament de Skylines
            for posicio, bloc in enumerate(self.edificis):
                self.edificis[posicio][0] = self.edificis[posicio][0] - other
                self.edificis[posicio][2] = self.edificis[posicio][2] - other
            return self
