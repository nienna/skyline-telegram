# Generated from Skyline.g by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .SkylineParser import SkylineParser
else:
    from SkylineParser import SkylineParser

# This class defines a complete generic visitor for a parse tree produced by SkylineParser.


class SkylineVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by SkylineParser#root.
    def visitRoot(self, ctx: SkylineParser.RootContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#expr.
    def visitExpr(self, ctx: SkylineParser.ExprContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#mirror.
    def visitMirror(self, ctx: SkylineParser.MirrorContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#asignation.
    def visitAsignation(self, ctx: SkylineParser.AsignationContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#skyline.
    def visitSkyline(self, ctx: SkylineParser.SkylineContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#creation.
    def visitCreation(self, ctx: SkylineParser.CreationContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#sklcreation.
    def visitSklcreation(self, ctx: SkylineParser.SklcreationContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#sklcrtrand.
    def visitSklcrtrand(self, ctx: SkylineParser.SklcrtrandContext):
        return self.visitChildren(ctx)


del SkylineParser