if __name__ is not None and "." in __name__:
    from .SkylineParser import SkylineParser
    from .SkylineVisitor import SkylineVisitor
else:
    from SkylineParser import SkylineParser
    from SkylineVisitor import SkylineVisitor


class TreeEval(SkylineVisitor):
    def __init__(self):
        self.operations = []

    def getOperations(self):
        return self.operations

    def importantchar(self, c):
        switch = {
            "LPAR(()": False,
            "RPAR())": False,
            "CBRK([)": False,
            "OBRK(])": False,
            "CBRKO({)": False,
            "CBRKC(})": False,
            "COMA(,)": False
        }
        return switch.get(c, True)

    # Visit a parse tree produced by SkylineParser#expr.
    def visitExpr(self, ctx: SkylineParser.ExprContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                char = SkylineParser.symbolicNames[aux.getSymbol().type] + '(' + aux.getText() + ')'
                if self.importantchar(char):
                    self.operations.append(char)
        self.visitChildren(ctx)
        return

    # Visit a parse tree produced by SkylineParser#mirror.
    def visitMirror(self, ctx: SkylineParser.MirrorContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                self.operations.append("mirror")
        self.visitChildren(ctx)
        return

    # Visit a parse tree produced by SkylineParser#asignation.
    def visitAsignation(self, ctx: SkylineParser.AsignationContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                char = SkylineParser.symbolicNames[aux.getSymbol().type] + '(' + aux.getText() + ')'
                if self.importantchar(char):
                    self.operations.append(char)
        self.visitChildren(ctx)
        return

    # Visit a parse tree produced by SkylineParser#skyline.
    def visitSkyline(self, ctx: SkylineParser.SkylineContext):
        self.operations.append("skyline")
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                char = SkylineParser.symbolicNames[aux.getSymbol().type] + '(' + aux.getText() + ')'
                if self.importantchar(char):
                    self.operations.append(char)

        self.visitChildren(ctx)
        return

    # Visit a parse tree produced by SkylineParser#creation.
    def visitCreation(self, ctx: SkylineParser.CreationContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                char = SkylineParser.symbolicNames[aux.getSymbol().type] + '(' + aux.getText() + ')'
                if char == "CBRK([)":
                    self.operations.append("creation_composta")
                if self.importantchar(char):
                    self.operations.append(char)
        self.visitChildren(ctx)

    # Visit a parse tree produced by SkylineParser#sklcreation.
    def visitSklcreation(self, ctx: SkylineParser.SklcreationContext):
        self.operations.append("creation_simple")
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                char = SkylineParser.symbolicNames[aux.getSymbol().type] + '(' + aux.getText() + ')'
                if self.importantchar(char):
                    self.operations.append(char)
        self.visitChildren(ctx)
        return

    # Visit a parse tree produced by SkylineParser#sklcrtrand.
    def visitSklcrtrand(self, ctx: SkylineParser.SklcrtrandContext):
        self.operations.append("creation_random")
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                char = SkylineParser.symbolicNames[aux.getSymbol().type] + '(' + aux.getText() + ')'
                if self.importantchar(char):
                    self.operations.append(char)
        self.visitChildren(ctx)
        return
