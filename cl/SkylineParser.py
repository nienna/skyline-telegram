# Generated from Skyline.g by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20")
        buf.write("j\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\5\3\33\n")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\5\3\"\n\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\62\n\3\7\3\64\n")
        buf.write("\3\f\3\16\3\67\13\3\3\4\3\4\3\4\3\4\5\4=\n\4\3\5\3\5\3")
        buf.write("\5\3\5\3\6\3\6\5\6E\n\6\3\7\3\7\3\7\3\7\7\7K\n\7\f\7\16")
        buf.write("\7N\13\7\3\7\3\7\3\7\3\7\5\7T\n\7\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\t\3\t\2\3\4\n\2\4\6\b\n\f\16\20\2\2\2q\2\22\3\2\2")
        buf.write("\2\4!\3\2\2\2\6<\3\2\2\2\b>\3\2\2\2\nD\3\2\2\2\fS\3\2")
        buf.write("\2\2\16U\3\2\2\2\20]\3\2\2\2\22\23\5\4\3\2\23\24\7\2\2")
        buf.write("\3\24\3\3\2\2\2\25\26\b\3\1\2\26\27\7\t\2\2\27\30\5\4")
        buf.write("\3\2\30\32\7\n\2\2\31\33\5\4\3\2\32\31\3\2\2\2\32\33\3")
        buf.write("\2\2\2\33\"\3\2\2\2\34\"\5\6\4\2\35\"\5\n\6\2\36\"\5\b")
        buf.write("\5\2\37\"\5\n\6\2 \"\7\4\2\2!\25\3\2\2\2!\34\3\2\2\2!")
        buf.write("\35\3\2\2\2!\36\3\2\2\2!\37\3\2\2\2! \3\2\2\2\"\65\3\2")
        buf.write("\2\2#$\f\t\2\2$%\7\5\2\2%\64\5\4\3\n&\'\f\b\2\2\'(\7\6")
        buf.write("\2\2(\64\5\4\3\t)*\f\7\2\2*+\7\7\2\2+\64\5\4\3\b,-\f\13")
        buf.write("\2\2-.\7\t\2\2./\5\4\3\2/\61\7\n\2\2\60\62\5\4\3\2\61")
        buf.write("\60\3\2\2\2\61\62\3\2\2\2\62\64\3\2\2\2\63#\3\2\2\2\63")
        buf.write("&\3\2\2\2\63)\3\2\2\2\63,\3\2\2\2\64\67\3\2\2\2\65\63")
        buf.write("\3\2\2\2\65\66\3\2\2\2\66\5\3\2\2\2\67\65\3\2\2\289\7")
        buf.write("\7\2\29=\5\n\6\2:;\7\7\2\2;=\5\4\3\2<8\3\2\2\2<:\3\2\2")
        buf.write("\2=\7\3\2\2\2>?\5\n\6\2?@\7\b\2\2@A\5\4\3\2A\t\3\2\2\2")
        buf.write("BE\7\3\2\2CE\5\f\7\2DB\3\2\2\2DC\3\2\2\2E\13\3\2\2\2F")
        buf.write("G\7\f\2\2GL\5\16\b\2HI\7\17\2\2IK\5\16\b\2JH\3\2\2\2K")
        buf.write("N\3\2\2\2LJ\3\2\2\2LM\3\2\2\2MO\3\2\2\2NL\3\2\2\2OP\7")
        buf.write("\13\2\2PT\3\2\2\2QT\5\16\b\2RT\5\20\t\2SF\3\2\2\2SQ\3")
        buf.write("\2\2\2SR\3\2\2\2T\r\3\2\2\2UV\7\t\2\2VW\7\4\2\2WX\7\17")
        buf.write("\2\2XY\7\4\2\2YZ\7\17\2\2Z[\7\4\2\2[\\\7\n\2\2\\\17\3")
        buf.write("\2\2\2]^\7\r\2\2^_\7\4\2\2_`\7\17\2\2`a\7\4\2\2ab\7\17")
        buf.write("\2\2bc\7\4\2\2cd\7\17\2\2de\7\4\2\2ef\7\17\2\2fg\7\4\2")
        buf.write("\2gh\7\16\2\2h\21\3\2\2\2\13\32!\61\63\65<DLS")
        return buf.getvalue()


class SkylineParser ( Parser ):

    grammarFileName = "Skyline.g"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "'*'", "'+'", 
                     "'-'", "':='", "'('", "')'", "']'", "'['", "'{'", "'}'", 
                     "','" ]

    symbolicNames = [ "<INVALID>", "WORD", "NUM", "MULT", "ADD", "SUB", 
                      "ASIG", "LPAR", "RPAR", "OBRK", "CBRK", "CBRKO", "CBRKC", 
                      "COMA", "WS" ]

    RULE_root = 0
    RULE_expr = 1
    RULE_mirror = 2
    RULE_asignation = 3
    RULE_skyline = 4
    RULE_creation = 5
    RULE_sklcreation = 6
    RULE_sklcrtrand = 7

    ruleNames =  [ "root", "expr", "mirror", "asignation", "skyline", "creation", 
                   "sklcreation", "sklcrtrand" ]

    EOF = Token.EOF
    WORD=1
    NUM=2
    MULT=3
    ADD=4
    SUB=5
    ASIG=6
    LPAR=7
    RPAR=8
    OBRK=9
    CBRK=10
    CBRKO=11
    CBRKC=12
    COMA=13
    WS=14

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class RootContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(SkylineParser.ExprContext,0)


        def EOF(self):
            return self.getToken(SkylineParser.EOF, 0)

        def getRuleIndex(self):
            return SkylineParser.RULE_root

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRoot" ):
                return visitor.visitRoot(self)
            else:
                return visitor.visitChildren(self)




    def root(self):

        localctx = SkylineParser.RootContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_root)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 16
            self.expr(0)
            self.state = 17
            self.match(SkylineParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAR(self):
            return self.getToken(SkylineParser.LPAR, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkylineParser.ExprContext)
            else:
                return self.getTypedRuleContext(SkylineParser.ExprContext,i)


        def RPAR(self):
            return self.getToken(SkylineParser.RPAR, 0)

        def mirror(self):
            return self.getTypedRuleContext(SkylineParser.MirrorContext,0)


        def skyline(self):
            return self.getTypedRuleContext(SkylineParser.SkylineContext,0)


        def asignation(self):
            return self.getTypedRuleContext(SkylineParser.AsignationContext,0)


        def NUM(self):
            return self.getToken(SkylineParser.NUM, 0)

        def MULT(self):
            return self.getToken(SkylineParser.MULT, 0)

        def ADD(self):
            return self.getToken(SkylineParser.ADD, 0)

        def SUB(self):
            return self.getToken(SkylineParser.SUB, 0)

        def getRuleIndex(self):
            return SkylineParser.RULE_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = SkylineParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 2
        self.enterRecursionRule(localctx, 2, self.RULE_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 31
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.state = 20
                self.match(SkylineParser.LPAR)
                self.state = 21
                self.expr(0)
                self.state = 22
                self.match(SkylineParser.RPAR)
                self.state = 24
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
                if la_ == 1:
                    self.state = 23
                    self.expr(0)


                pass

            elif la_ == 2:
                self.state = 26
                self.mirror()
                pass

            elif la_ == 3:
                self.state = 27
                self.skyline()
                pass

            elif la_ == 4:
                self.state = 28
                self.asignation()
                pass

            elif la_ == 5:
                self.state = 29
                self.skyline()
                pass

            elif la_ == 6:
                self.state = 30
                self.match(SkylineParser.NUM)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 51
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 49
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
                    if la_ == 1:
                        localctx = SkylineParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 33
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 34
                        self.match(SkylineParser.MULT)
                        self.state = 35
                        self.expr(8)
                        pass

                    elif la_ == 2:
                        localctx = SkylineParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 36
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 37
                        self.match(SkylineParser.ADD)
                        self.state = 38
                        self.expr(7)
                        pass

                    elif la_ == 3:
                        localctx = SkylineParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 39
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 40
                        self.match(SkylineParser.SUB)
                        self.state = 41
                        self.expr(6)
                        pass

                    elif la_ == 4:
                        localctx = SkylineParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 42
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 43
                        self.match(SkylineParser.LPAR)
                        self.state = 44
                        self.expr(0)
                        self.state = 45
                        self.match(SkylineParser.RPAR)
                        self.state = 47
                        self._errHandler.sync(self)
                        la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
                        if la_ == 1:
                            self.state = 46
                            self.expr(0)


                        pass

             
                self.state = 53
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class MirrorContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SUB(self):
            return self.getToken(SkylineParser.SUB, 0)

        def skyline(self):
            return self.getTypedRuleContext(SkylineParser.SkylineContext,0)


        def expr(self):
            return self.getTypedRuleContext(SkylineParser.ExprContext,0)


        def getRuleIndex(self):
            return SkylineParser.RULE_mirror

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMirror" ):
                return visitor.visitMirror(self)
            else:
                return visitor.visitChildren(self)




    def mirror(self):

        localctx = SkylineParser.MirrorContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_mirror)
        try:
            self.state = 58
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 54
                self.match(SkylineParser.SUB)
                self.state = 55
                self.skyline()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 56
                self.match(SkylineParser.SUB)
                self.state = 57
                self.expr(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class AsignationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def skyline(self):
            return self.getTypedRuleContext(SkylineParser.SkylineContext,0)


        def ASIG(self):
            return self.getToken(SkylineParser.ASIG, 0)

        def expr(self):
            return self.getTypedRuleContext(SkylineParser.ExprContext,0)


        def getRuleIndex(self):
            return SkylineParser.RULE_asignation

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAsignation" ):
                return visitor.visitAsignation(self)
            else:
                return visitor.visitChildren(self)




    def asignation(self):

        localctx = SkylineParser.AsignationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_asignation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 60
            self.skyline()
            self.state = 61
            self.match(SkylineParser.ASIG)
            self.state = 62
            self.expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SkylineContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WORD(self):
            return self.getToken(SkylineParser.WORD, 0)

        def creation(self):
            return self.getTypedRuleContext(SkylineParser.CreationContext,0)


        def getRuleIndex(self):
            return SkylineParser.RULE_skyline

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSkyline" ):
                return visitor.visitSkyline(self)
            else:
                return visitor.visitChildren(self)




    def skyline(self):

        localctx = SkylineParser.SkylineContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_skyline)
        try:
            self.state = 66
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SkylineParser.WORD]:
                self.enterOuterAlt(localctx, 1)
                self.state = 64
                self.match(SkylineParser.WORD)
                pass
            elif token in [SkylineParser.LPAR, SkylineParser.CBRK, SkylineParser.CBRKO]:
                self.enterOuterAlt(localctx, 2)
                self.state = 65
                self.creation()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CreationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CBRK(self):
            return self.getToken(SkylineParser.CBRK, 0)

        def sklcreation(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(SkylineParser.SklcreationContext)
            else:
                return self.getTypedRuleContext(SkylineParser.SklcreationContext,i)


        def OBRK(self):
            return self.getToken(SkylineParser.OBRK, 0)

        def COMA(self, i:int=None):
            if i is None:
                return self.getTokens(SkylineParser.COMA)
            else:
                return self.getToken(SkylineParser.COMA, i)

        def sklcrtrand(self):
            return self.getTypedRuleContext(SkylineParser.SklcrtrandContext,0)


        def getRuleIndex(self):
            return SkylineParser.RULE_creation

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCreation" ):
                return visitor.visitCreation(self)
            else:
                return visitor.visitChildren(self)




    def creation(self):

        localctx = SkylineParser.CreationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_creation)
        self._la = 0 # Token type
        try:
            self.state = 81
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [SkylineParser.CBRK]:
                self.enterOuterAlt(localctx, 1)
                self.state = 68
                self.match(SkylineParser.CBRK)
                self.state = 69
                self.sklcreation()
                self.state = 74
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==SkylineParser.COMA:
                    self.state = 70
                    self.match(SkylineParser.COMA)
                    self.state = 71
                    self.sklcreation()
                    self.state = 76
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 77
                self.match(SkylineParser.OBRK)
                pass
            elif token in [SkylineParser.LPAR]:
                self.enterOuterAlt(localctx, 2)
                self.state = 79
                self.sklcreation()
                pass
            elif token in [SkylineParser.CBRKO]:
                self.enterOuterAlt(localctx, 3)
                self.state = 80
                self.sklcrtrand()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SklcreationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LPAR(self):
            return self.getToken(SkylineParser.LPAR, 0)

        def NUM(self, i:int=None):
            if i is None:
                return self.getTokens(SkylineParser.NUM)
            else:
                return self.getToken(SkylineParser.NUM, i)

        def COMA(self, i:int=None):
            if i is None:
                return self.getTokens(SkylineParser.COMA)
            else:
                return self.getToken(SkylineParser.COMA, i)

        def RPAR(self):
            return self.getToken(SkylineParser.RPAR, 0)

        def getRuleIndex(self):
            return SkylineParser.RULE_sklcreation

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSklcreation" ):
                return visitor.visitSklcreation(self)
            else:
                return visitor.visitChildren(self)




    def sklcreation(self):

        localctx = SkylineParser.SklcreationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_sklcreation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self.match(SkylineParser.LPAR)
            self.state = 84
            self.match(SkylineParser.NUM)
            self.state = 85
            self.match(SkylineParser.COMA)
            self.state = 86
            self.match(SkylineParser.NUM)
            self.state = 87
            self.match(SkylineParser.COMA)
            self.state = 88
            self.match(SkylineParser.NUM)
            self.state = 89
            self.match(SkylineParser.RPAR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SklcrtrandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CBRKO(self):
            return self.getToken(SkylineParser.CBRKO, 0)

        def NUM(self, i:int=None):
            if i is None:
                return self.getTokens(SkylineParser.NUM)
            else:
                return self.getToken(SkylineParser.NUM, i)

        def COMA(self, i:int=None):
            if i is None:
                return self.getTokens(SkylineParser.COMA)
            else:
                return self.getToken(SkylineParser.COMA, i)

        def CBRKC(self):
            return self.getToken(SkylineParser.CBRKC, 0)

        def getRuleIndex(self):
            return SkylineParser.RULE_sklcrtrand

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSklcrtrand" ):
                return visitor.visitSklcrtrand(self)
            else:
                return visitor.visitChildren(self)




    def sklcrtrand(self):

        localctx = SkylineParser.SklcrtrandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_sklcrtrand)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 91
            self.match(SkylineParser.CBRKO)
            self.state = 92
            self.match(SkylineParser.NUM)
            self.state = 93
            self.match(SkylineParser.COMA)
            self.state = 94
            self.match(SkylineParser.NUM)
            self.state = 95
            self.match(SkylineParser.COMA)
            self.state = 96
            self.match(SkylineParser.NUM)
            self.state = 97
            self.match(SkylineParser.COMA)
            self.state = 98
            self.match(SkylineParser.NUM)
            self.state = 99
            self.match(SkylineParser.COMA)
            self.state = 100
            self.match(SkylineParser.NUM)
            self.state = 101
            self.match(SkylineParser.CBRKC)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[1] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 9)
         




