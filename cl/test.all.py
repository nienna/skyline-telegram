import sys
from antlr4 import *
from SkylineLexer import SkylineLexer
from SkylineParser import SkylineParser
from TreeVisitor import TreeVisitor

print("Test ha efectuar?")
opcio = int(input("1)Skylines 2)Operacions 3)Inversions 4)Asignacions 5)Parentesis 6)Tot  "))

if opcio == 1 or opcio == 6:
    print("Skyline")
    print("###################################")
    with open('test/skyline.test') as f:
        for line in f:
            print(line)
            input_stream = InputStream(line)

            lexer = SkylineLexer(input_stream)
            token_stream = CommonTokenStream(lexer)
            parser = SkylineParser(token_stream)
            tree = parser.root()

            visitor = TreeVisitor()
            visitor.visit(tree)
            print('----------')

if opcio == 2 or opcio == 6:
    print("Operacions")
    print("###################################")
    with open('test/operation.test') as f:
        for line in f:
            print(line)
            input_stream = InputStream(line)

            lexer = SkylineLexer(input_stream)
            token_stream = CommonTokenStream(lexer)
            parser = SkylineParser(token_stream)
            tree = parser.root()

            visitor = TreeVisitor()
            visitor.visit(tree)
            print('----------')

if opcio == 3 or opcio == 6:
    print("Inversions")
    print("###################################")
    with open('test/mirror.test') as f:
        for line in f:
            print(line)
            input_stream = InputStream(line)

            lexer = SkylineLexer(input_stream)
            token_stream = CommonTokenStream(lexer)
            parser = SkylineParser(token_stream)
            tree = parser.root()

            visitor = TreeVisitor()
            visitor.visit(tree)
            print('----------')
if opcio == 4 or opcio == 6:
    print("Asignacions")
    print("###################################")
    with open('test/asignation.test') as f:
        for line in f:
            print(line)
            input_stream = InputStream(line)

            lexer = SkylineLexer(input_stream)
            token_stream = CommonTokenStream(lexer)
            parser = SkylineParser(token_stream)
            tree = parser.root()

            visitor = TreeVisitor()
            visitor.visit(tree)
            print('----------')

if opcio == 5 or opcio == 6:
    print("Parentesis")
    print("###################################")
    with open('test/parentesis.test') as f:
        for line in f:
            print(line)
            input_stream = InputStream(line)

            lexer = SkylineLexer(input_stream)
            token_stream = CommonTokenStream(lexer)
            parser = SkylineParser(token_stream)
            tree = parser.root()

            visitor = TreeVisitor()
            visitor.visit(tree)
            print('----------')