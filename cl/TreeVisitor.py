if __name__ is not None and "." in __name__:
    from .SkylineParser import SkylineParser
    from .SkylineVisitor import SkylineVisitor
else:
    from SkylineParser import SkylineParser
    from SkylineVisitor import SkylineVisitor


class TreeVisitor(SkylineVisitor):
    def __init__(self):
        self.nivell = 0

    def visitExpr(self, ctx: SkylineParser.ExprContext):
        n = ctx.getChildCount()
        subeval = False
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                print("  " * self.nivell +
                      SkylineParser.symbolicNames[aux.getSymbol().type] +
                      '(' + aux.getText() + ')')
            elif not subeval:
                self.nivell += 1
                self.visitChildren(ctx)
                self.nivell -= 1
                subeval = True
        return

    # Visit a parse tree produced by SkylineParser#mirror.
    def visitMirror(self, ctx: SkylineParser.MirrorContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                print("  " * self.nivell + "mirror")
        self.nivell += 1
        self.visitChildren(ctx)
        self.nivell -= 1
        return

    # Visit a parse tree produced by SkylineParser#asignation.
    def visitAsignation(self, ctx: SkylineParser.AsignationContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                print("  " * self.nivell +
                      SkylineParser.symbolicNames[aux.getSymbol().type] +
                      '(' + aux.getText() + ')')
        self.nivell += 1
        self.visitChildren(ctx)
        self.nivell -= 1
        return

    # Visit a parse tree produced by SkylineParser#skyline.
    def visitSkyline(self, ctx: SkylineParser.SkylineContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                print("  " * self.nivell +
                      SkylineParser.symbolicNames[aux.getSymbol().type] +
                      '(' + aux.getText() + ')')
            else:
                print("  " * self.nivell + "skyline")
        self.nivell += 1
        self.visitChildren(ctx)
        self.nivell -= 1
        return

    # Visit a parse tree produced by SkylineParser#creation.
    def visitCreation(self, ctx: SkylineParser.CreationContext):
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            if aux.getChildCount() == 0:
                print("  " * self.nivell +
                      SkylineParser.symbolicNames[aux.getSymbol().type] +
                      '(' + aux.getText() + ')')
            else:
                print("  " * self.nivell + "skylinecreation")
        self.nivell += 1
        self.visitChildren(ctx)
        self.nivell -= 1
        return

    # Visit a parse tree produced by SkylineParser#sklcreation.
    def visitSklcreation(self, ctx: SkylineParser.SklcreationContext):
        n = ctx.getChildCount()
        self.nivell += 1
        for i in range(n):
            aux = ctx.getChild(i)
            print("  " * self.nivell +
                  SkylineParser.symbolicNames[aux.getSymbol().type] +
                  '(' + aux.getText() + ')')
        self.visitChildren(ctx)
        self.nivell -= 1
        return

    # Visit a parse tree produced by SkylineParser#sklcrtrand.
    def visitSklcrtrand(self, ctx: SkylineParser.SklcrtrandContext):
        self.nivell += 1
        n = ctx.getChildCount()
        for i in range(n):
            aux = ctx.getChild(i)
            print("  " * self.nivell +
                  SkylineParser.symbolicNames[aux.getSymbol().type] +
                  '(' + aux.getText() + ')')
        self.visitChildren(ctx)
        self.nivell -= 1
        return
