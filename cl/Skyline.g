grammar Skyline;

root : expr EOF ;

expr : LPAR expr RPAR (expr)?
    | expr LPAR expr RPAR (expr)?
    | mirror
    | expr MULT expr
    | expr ADD expr
    | expr SUB expr
    | skyline
    | asignation
    | skyline
    | NUM;

mirror: SUB skyline
    | SUB expr;

asignation : skyline ASIG expr;

skyline: WORD
    | creation;

creation : CBRK sklcreation (COMA sklcreation)* OBRK
    | sklcreation
    | sklcrtrand;

sklcreation : LPAR NUM COMA NUM COMA NUM RPAR ;
sklcrtrand : CBRKO NUM COMA NUM COMA NUM COMA NUM COMA NUM CBRKC;


WORD :  [a-zA-Z] ([a-zA-Z0-9]+)? ;
NUM : [0-9]+ ;
MULT : '*' ;
ADD : '+' ;
SUB :'-' ;
ASIG : ':=' ;
LPAR : '(' ;
RPAR : ')' ;
OBRK : ']' ;
CBRK : '[' ;
CBRKO : '{' ;
CBRKC : '}' ;
COMA : ',' ;
WS : [ \n]+ -> skip ;
