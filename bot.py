from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from skyline import skyline
import pickle
import matplotlib.pyplot as plt
import logging
import copy
import os  # Asumim que nomes s'executara a Linux
from cl.SkylineLexer import SkylineLexer
from cl.SkylineParser import SkylineParser
from cl.TreeEval import TreeEval
from antlr4 import *

logger = logging.getLogger("skyline.bot")
help_string = """/start inicia la conversa amb el Bot.
/help el Bot ha de contestar amb una llista de totes les possibles comandes i una breu documentació sobre el seu propòsit i ús.
/author el Bot ha d’escriure el nom de l’autor del projecte
/lst: mostra els identificadors definits i la seva corresponent àrea.
/clean: esborra tots els identificadors definits.
/save id: ha de guardar un skyline definit amb el nom id.sky.
/load id: ha de carregar un skyline de l’arxiu id.sky."""


llistaskylines = {}
itoper = 0


# Funcio de la commanda start
def start_command(update, context):
    # Asumim que el bot es mante sempre en funcionament aixi es mantenen els dics dels usuaris
    idusr = str(update.message.from_user.id)
    #  Cada usuari te una entrada al diccionari formada per un altre diccionari
    logger.info("New user connected with id:{idusr}".format(idusr=idusr))
    llistaskylines[idusr] = {}
    update.message.reply_text(
        'Benvingut/da {}!'.format(update.message.from_user.first_name))


# Funcio de la commanda help
def help_command(update, context):
    update.message.reply_text(help_string)


# Funcio de la commanda author
def author_command(update, context):
    update.message.reply_text(
        'Nienna')


# Funcio de la commanda list
def lst_command(update, context):
    idusr = str(update.message.from_user.id)
    messagereturn = ""
    for key, skyln in llistaskylines[idusr].items():
        messagereturn = messagereturn + key + " "
        messagereturn = messagereturn + str(skyln.area) + "\n"
    if len(llistaskylines[idusr]) == 0:
        update.message.reply_text("Sense identificadors definits")
    else:
        update.message.reply_text(messagereturn)


# Funcio de la commanda Save
def clean_command(update, context):
    global llistaskylines
    idusr = str(update.message.from_user.id)
    del llistaskylines[idusr]
    llistaskylines[idusr] = {}
    update.message.reply_text(
        'Eliminats tots els identificadors definits')


# Funcio de la commanda save
def save_command(update, context):
    text = update.message.text
    text = text.split(" ")
    idusr = str(update.message.from_user.id)
    path = "./usersaves/" + idusr  # Es guarda al dir del bot
    mode = 0o760  # Permisos RWX,RW,-
    try:
        os.mkdir(path, mode)
    except OSError as exc:  # Si el directori ja existeix llença un error
        #  17 es el codi d'error de que el directory existeix
        if exc.errno == 17 and os.path.isdir(path):
            pass
        else:
            raise
    with open(path + "/" + text[1] + ".sky", 'wb') as f:
        pickle.dump(llistaskylines, f)
    update.message.reply_text('Skyline guardat')


# Funcio de la commanda load
def load_command(update, context):
    try:
        global llistaskylines
        text = update.message.text
        text = text.split(" ")
        idusr = str(update.message.from_user.id)
        path = "./usersaves/" + idusr + "/"
        with open(path + text[1] + ".sky", 'rb') as f:
            llistaskylines = pickle.load(f)
        update.message.reply_text("Skyline carregat "+text)
    except IOError:
        update.message.reply_text("No s'ha trobat el fitxer")


# Funcio per evaluar les dades retornades per el visitor
def evaluar_operations(operations, idusr):
    global itoper
    skl = skyline()
    while itoper < len(operations):
        oper = operations[itoper]
        if oper == "skyline":
            skl = evaluar_skyline(operations, idusr)
        elif oper == "ASIG(:=)":
            skl = evaluar_asig(operations, idusr)
        elif oper == "mirror":
            skl = evaluar_inversio(operations, idusr)
        elif oper == "MULT(*)":
            skl = evaluar_mult(operations, idusr)
        elif oper == "ADD(+)":
            skl = evaluar_suma(operations, idusr)
        elif oper == "SUB(-)":
            skl = evaluar_resta(operations, idusr)
        else:
            return "Null"
        itoper += 1
    return skl


# Funcio per extreure el valor de les dades retornades per el visitor
def extractvalue(str):
    pair = str.split("(")
    if pair[0] == "NUM":
        aux = pair[1].split(")")
        return int(aux[0])
    else:
        aux = pair[1].split(")")
        return aux[0]


# Funcio per tractar la creacio de skylines
def evaluar_skyline(operations, idusr):
    global itoper
    itoper += 1
    oper = operations[itoper]
    if oper == "creation_simple":
        skl = skyline()
        xmin = extractvalue(operations[itoper+1])
        altura = extractvalue(operations[itoper+2])
        xmax = extractvalue(operations[itoper+3])
        itoper = itoper + 3
        skl.creacio_unica(xmin, altura, xmax)
        return skl
    elif oper == "creation_composta":
        skl = skyline()
        skllist = []
        itoper += 1
        while operations[itoper] == "creation_simple":
            xmin = extractvalue(operations[itoper+1])
            altura = extractvalue(operations[itoper+2])
            xmax = extractvalue(operations[itoper+3])
            skllist.append([xmin, altura, xmax])
            itoper += 4
            if itoper >= len(operations):
                skl.creacio_composta(skllist)
                return skl
        skl.creacio_composta(skllist)
        return skl
    elif oper == "creation_random":
        skl = skyline()
        n = extractvalue(operations[itoper+1])
        h = extractvalue(operations[itoper+2])
        w = extractvalue(operations[itoper+3])
        xmin = extractvalue(operations[itoper+4])
        xmax = extractvalue(operations[itoper+5])
        itoper = itoper + 5
        skl.creacio_aleatoria(n, h, w, xmin, xmax)
        return skl
    elif "WORD" in oper:
        ids = extractvalue(operations[itoper])
        if idusr in llistaskylines and ids in llistaskylines[idusr]:
            skl = copy.deepcopy(llistaskylines[idusr][ids])
        else:
            skl = "Empty"
        return skl


# Funcio per tractar les inversions
def evaluar_inversio(operations, idusr):
    global itoper
    itoper += 1
    aux1 = 0
    if operations[itoper] == "MULT(*)":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux1 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux1 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux1 = evaluar_skyline(operations, idusr)
    aux1.invertir_skyline()
    return aux1


# Funcio per tractar les multiplicacio
def evaluar_mult(operations, idusr):
    global itoper
    itoper += 1
    aux1 = 0
    aux2 = 0
    if "NUM" in operations[itoper]:
        aux1 = extractvalue(operations[itoper])
    elif operations[itoper] == "MULT(*)":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux1 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux1 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux1 = evaluar_skyline(operations, idusr)
    itoper += 1
    if "NUM" in operations[itoper]:
        aux2 = extractvalue(operations[itoper])
    elif operations[itoper] == "MULT(*)":
        aux2 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux2 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux2 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux2 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux2 = evaluar_skyline(operations, idusr)
    return aux1 * aux2


# Funcio per tractar les sumes
def evaluar_suma(operations, idusr):
    global itoper
    itoper += 1
    aux1 = 0
    aux2 = 0
    if "NUM" in operations[itoper]:
        aux1 = extractvalue(operations[itoper])
    elif operations[itoper] == "MULT(*)":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux1 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux1 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux1 = evaluar_skyline(operations, idusr)
    itoper += 1
    if "NUM" in operations[itoper]:
        aux2 = extractvalue(operations[itoper])
    elif operations[itoper] == "MULT(*)":
        aux2 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux2 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux2 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux2 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux2 = evaluar_skyline(operations, idusr)
    return aux1 + aux2


# Funcio per tractar les Restes
def evaluar_resta(operations, idusr):
    global itoper
    itoper += 1
    aux1 = 0
    aux2 = 0
    if "NUM" in operations[itoper]:
        aux1 = extractvalue(operations[itoper])
    elif operations[itoper] == "MULT(*)":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux1 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux1 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux1 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux1 = evaluar_skyline(operations, idusr)
    itoper += 1
    if "NUM" in operations[itoper]:
        aux2 = extractvalue(operations[itoper])
    elif operations[itoper] == "MULT(*)":
        aux2 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "ADD(+)":
        aux2 = evaluar_suma(operations, idusr)
    elif operations[itoper] == "SUB(-)":
        aux2 = evaluar_resta(operations, idusr)
    elif operations[itoper] == "mirror":
        aux2 = evaluar_mult(operations, idusr)
    elif operations[itoper] == "skyline":
        aux2 = evaluar_skyline(operations, idusr)
    return aux1 - aux2


# Funcio per tractar les asignacions
def evaluar_asig(operations, idusr):
    global itoper
    itoper += 1
    if operations[itoper] != "skyline":
        return -1
    itoper += 1
    if "WORD" not in operations[itoper]:
        return -1
    aux1 = extractvalue(operations[itoper])
    #  Evaluem la part esquerra de la asignacio de manera independent
    itoper += 1
    aux2 = evaluar_operations(operations, idusr)
    itoper
    global llistaskylines
    if idusr not in llistaskylines:
        print("Crear dict")
        llistaskylines[idusr] = {}
    llistaskylines[idusr][aux1] = aux2
    return aux2


# Tractament de tots els missatges que no son commandes
def any_message(update, context):
    input_stream = InputStream(update.message.text)

    lexer = SkylineLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = SkylineParser(token_stream)
    tree = parser.root()

    visitor = TreeEval()
    visitor.visit(tree)
    operations = visitor.getOperations()

    logger.debug("Oper list returned: {x}".format(x=operations))
    global itoper
    itoper = 0
    idusr = str(update.message.from_user.id)
    show = evaluar_operations(operations, idusr)
    if show == "Null":
        update.message.reply_text("Missatge escrit incorrectament")
    elif show == "Empty":
        update.message.reply_text("Variable inexistent")
    else:
        plt.clf()
        plt.axes()
        for edifici in show.edificis:
            rectangle = plt.Rectangle((edifici[0], 0), edifici[2]-edifici[0], edifici[1], fc='red', ec="blue")
            plt.gca().add_patch(rectangle)
        plt.axis('scaled')
        plt.savefig('plot.png')
        context.bot.send_photo(chat_id=update.effective_chat.id, photo=open('plot.png', 'rb'))
        update.message.reply_text('Area: {a}, Alçada: {h}'.format(a=show.area, h=show.altura))


# Inici del bot i declaracio de les commandes
try:
    token = ""
    with open('token.txt', 'r') as f:
        token = f.read(45)
    logger.info("Valid token ({x}) loaded".format(x=token))
    updater = Updater(token, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start_command))
    updater.dispatcher.add_handler(CommandHandler('help', help_command))
    updater.dispatcher.add_handler(CommandHandler('author', author_command))
    updater.dispatcher.add_handler(CommandHandler('lst', lst_command))
    updater.dispatcher.add_handler(CommandHandler('clean', clean_command))
    updater.dispatcher.add_handler(CommandHandler('save', save_command))
    updater.dispatcher.add_handler(CommandHandler('load', load_command))

    # Correct below for Pep8 compliance
    updater.dispatcher.add_handler(MessageHandler(Filters.text & (~Filters.command), any_message))

    updater.start_polling()
    updater.idle()
except IOError:
    logger.error("Error: Token file does not appear to exist.")
